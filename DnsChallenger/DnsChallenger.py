#!/usr/bin/python3

import boto3
import sys


class DnsChallenger:

    def __init__(self):
        self.dns = boto3.client('route53')

    def place_acme_challenge(self, hosted_zone, domain, challenge):
        zone_id = self.dns.list_hosted_zones_by_name(DNSName=hosted_zone)['HostedZones'][0]['Id']
        self.dns.change_resource_record_sets(
            HostedZoneId=zone_id,
            ChangeBatch={
                'Changes': [{
                    'Action': 'UPSERT', 'ResourceRecordSet': {
                        'Name':f"_acme-challenge.{domain}",
                        'Type': 'TXT',
                        'Weight': 1,
                        'SetIdentifier': f"acme-challenge-{domain}",
                        'TTL': 300,
                        'ResourceRecords': [{
                            'Value': f"\"{challenge}\""
                        }]
                    }
                }]
            }
        )


if __name__ == '__main__':
    #if len(sys.args) != 2
    # todo: 
    # add command line args for domain and challenge
    # in order to use the main function as hook.
    # see: https://hackernoon.com/easy-lets-encrypt-certificates-on-aws-79387767830b
    dc = DnsChallenger()
    dc.place_acme_challenge('mstiehr.de', 'secure-test2.mstiehr.de', 'my top secret challenge')

