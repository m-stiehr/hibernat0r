# Hibernat0r
Hibernate all ec2 instances that are tagged with the key `hibernate`.
Required environment variables: 
- REGION, e.g. eu-central-1 
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

# DnsChallenger
Place a ACME challenge for `Let's Encrypt`.
