""" notify via SNS """
import os
import boto3

class Notifier:
    """ notify subscribers via a SNS topic """

    def __init__(self, topic):
        self.topic = topic
        self.aws_profile = os.environ.get('AWS_PROFILE')
        self.region = os.environ.get('REGION')
        self.session = boto3.session.Session(profile_name=self.aws_profile, region_name=self.region)
        self.sns = self.session.client('sns')
        self.create_topic(topic)


    def create_topic(self, topic):
        """ create the topic """
        topic = self.sns.create_topic(Name=self.topic)
        self.topic_arn = topic['TopicArn']
        print(f"successfully (re-)created topic {self.topic_arn}")


    def notify(self, instances):
        """ notify via SNS """
        if len(instances) > 0:
            msg = f"Hibernating {len(instances)} instances: [{instances}]."
        else:
            msg = "No resources had to be hibernated."
        print(f"sending message: {msg}")
        response = self.sns.publish(
            TopicArn=self.topic_arn,
            Message=msg
        )
        print(response)
        print(f"successfully sent message: {response['MessageId']}")
