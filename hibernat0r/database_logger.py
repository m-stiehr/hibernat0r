""" save events in dynamodb """

import os
import time
import boto3
from botocore.exceptions import ClientError

class DatabaseLogger:
    """ save events in dynamodb """

    def __init__(self, table_name):
        aws_profile = os.environ.get('AWS_PROFILE')
        region = os.environ.get('REGION')
        self.table_name = table_name
        self.dynamo_db = boto3.session.Session(
            profile_name=aws_profile, region_name=region).resource('dynamodb')
        self.table = self.__init_table()

    def __init_table(self):
        """ initialize the table """
        tables = [ table.name for table in self.dynamo_db.tables.all()]
        if self.table_name in tables:
            print(f"table [{self.table_name}] exists")
            table = self.dynamo_db.Table(self.table_name)
        else:
            print("gotta create table")
            table = self.dynamo_db.create_table(
                TableName=self.table_name,
                KeySchema=[
                    {
                        'AttributeName': 'instance_id',
                        'KeyType': 'HASH'
                    }
                    # ,
                    # {
                    #     'AttributeName': 'timestamp',
                    #     'KeyType': 'RANGE'
                    # }
                ],
                AttributeDefinitions=[
                    # {
                    #     'AttributeName': 'timestamp',
                    #     'AttributeType': 'N'
                    # },
                    {
                        'AttributeName': 'instance_id',
                        'AttributeType': 'S'
                    }
                ],
                ProvisionedThroughput={
                    'ReadCapacityUnits': 1,
                    'WriteCapacityUnits': 1
                }
            )
            table.wait_until_exists()
            print(f"created new table: {self.table_name}")

        return table

    def list_entries(self):
        """ list all the entries """
        body = self.table.scan()
        items = body['Items']
        return items

    def store_message(self, identifier, message):
        """ store a message """
        self.table.put_item(
            Item={
                'instance_id': identifier,
                'payload': {
                    'message': message,
                    'timestamp': int(time.time()*10)
                }
            }
        )

    def query(self, identifier):
        """ query a message """
        try:
            response = self.table.get_item(Key={'instance_id': identifier})
            print(response)
            if 'Item' in response:
                return [response['Item']]
        except ClientError as error:
            print(error.response['Error']['Message'])

        return []

if __name__ == '__main__':
    db = DatabaseLogger('hibernat0r')
    print(db.query('my_instance123'))
