""" hibernate resources and nofify via sns """
import os
from hibernat0r import Hibernat0r
from sns_notifier import Notifier
from database_logger import DatabaseLogger

topic = os.environ.get('TOPIC')
hibernator = Hibernat0r()
notifier = Notifier(topic)
db_logger = DatabaseLogger(topic)

# pylint: disable=unused-argument
def handler(event, context):
    """ entrypoint for lambda """

    instances = hibernator.do_hibernate()
    notifier.notify(instances)

    for instance in instances:
        db_logger.store_message(instance, "was hibernated.")


if __name__ == '__main__':
    main_instances = hibernator.do_hibernate(dry_run=True)
    notifier.notify(main_instances)

    for i in main_instances:
        db_logger.store_message(i, "was hibernated from the main function.")
