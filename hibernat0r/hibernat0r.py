""" hibernate ec2 instances that are tagged with 'hibernate' """
import os
import boto3


class Hibernat0r:
    """ Hibernate ec2 instances that are tagged with 'hibernate' """

    def __init__(self):
        self.aws_profile = os.environ.get('AWS_PROFILE')
        self.region = os.environ.get('REGION')
        self.session = boto3.session.Session(profile_name=self.aws_profile, region_name=self.region)
        self.ec2 = self.session.client('ec2')

    def get_instances(self, filters):
        """ query instances using the filter """
        reservations = self.ec2.describe_instances(Filters=filters)['Reservations']
        instance_lists = [ r['Instances'] for r in reservations ]
        instances = [ instance for inner_list in instance_lists for instance in inner_list ]
        return instances

    def get_hibernate_instances(self):
        """ query instances """
        filters = [{
            'Name': 'instance-state-name',
            'Values': ['running']
        }, {
            'Name': 'tag-key',
            'Values': ['hibernate']
        }]
        return self.get_instances(filters)

    def get_hibernate_instance_ids(self):
        """ return the ids of instances """
        instances = self.get_hibernate_instances()
        # state = i['State']
        return [ instance['InstanceId'] for instance in instances ]

    def do_hibernate(self, dry_run=False):
        """ hibernate ec2 instances that are tagged with 'hibernate' """
        instance_ids = self.get_hibernate_instance_ids()
        if instance_ids:
            if dry_run:
                print("dry run - skipping")
            else:
                print(f"will now hibernate the following instances: {instance_ids}.")
                response = self.ec2.stop_instances(InstanceIds=instance_ids, Hibernate=True)
                for i in response['StoppingInstances']:
                    print(f"{i['InstanceId']}: {i['PreviousState']['Name']} ",
                           f"-> {i['CurrentState']['Name']}")
                print("done.")
        else:
            print("nothing to do.")

        return instance_ids
