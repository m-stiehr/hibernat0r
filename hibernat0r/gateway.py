""" access log files via api gateway """
import os
import json
from database_logger import DatabaseLogger

topic = os.environ.get('TOPIC')
db_logger = DatabaseLogger(topic)
# pylint: disable=unused-argument
def handler(event, context):
    """ entrypoint for lambda """
    print(f"event: {event}")
    print(f"context: {context}")

    if event['path'] == 'hibernator/all':
        print("looking for all items.")
        items = db_logger.list_entries()
    else:
        instance_id = event['queryStringParameters']['instance_id']
        print(f"looking for instance_id {instance_id}.")
        items = db_logger.query(instance_id)
    for item in items:
        item['payload']['timestamp'] = int(item['payload']['timestamp'])

    body = {'items': items}
    print(f"returning body: {body}")

    return {
        'statusCode': 200,
        'body': json.dumps(body)
    }


if __name__ == '__main__':
    main_items = db_logger.list_entries()
    main_body = {'items': main_items}
    for main_item in main_items:
        main_item['payload']['timestamp'] = int(main_item['payload']['timestamp'])
    print(json.dumps(main_body))
